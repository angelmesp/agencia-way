const express = require('express');
const router = express.Router();
const ventaController = require('../controllers/ventaController');

// Obtener todas las ventas
router.get('/ventas', ventaController.obtenerVentas);

// Crear venta
router.post('/ventas', ventaController.crearVenta);


router.get('/ventas/:id', ventaController.obtenerVentasID);


module.exports = router;

