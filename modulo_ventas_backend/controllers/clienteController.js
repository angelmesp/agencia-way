const mysql = require('mysql');

// Agregar cliente
exports.agregarCliente = (req, res) => {
  const { Nombre, Apellido, Email, Telefono } = req.body;
  const sql = 'CALL InsertarCliente(?, ?, ?, ?)';
  const values = [Nombre, Apellido, Email, Telefono];

   req.db.query(sql, values, (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al agregar el cliente' });
    } else {
        res.json({ message: 'Producto actualizado exitosamente' });
    }
  });
};


// Eliminar cliente
exports.eliminarCliente = (req, res) => {
  const { id } = req.params;
  const sql = 'CALL EliminarCliente(?)';

   req.db.query(sql, [id], (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al eliminar el cliente' });
    } else {
      res.json({ message: 'Cliente eliminado exitosamente' });
    }
  });
};

// Obtener todos los clientes
exports.obtenerClientes = (req, res) => {
  const sql = 'SELECT * FROM Cliente';
   req.db.query(sql, (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al obtener los clientes' });
    } else {
      res.json(result);
    }
  });
};
